/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ILC_service;

import ILC_service.Serial_port.Buf_class;
import java.io.UnsupportedEncodingException;
import static java.lang.Math.floor;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Base64;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class ILC_device_c {

    class sdStatus_class{
        long free; //Свободно
        long cap; //Емкость
        long fill; //Заполнено
    };
    
    //Статус устройства
    class devStatus_class{
        boolean ch1_active;
        boolean ch2_active;
        boolean ch3_active;
        boolean ch4_active;
        boolean sd_insert;
        boolean eth_link;
        boolean ems_link;
    }
    
    //Информация о файле
    class fileInfo_class{
        int fileSize;
        String fileName;
        String fileType;
    }
    
    fileInfo_class fileInfo = new fileInfo_class();
    devStatus_class devStatus = new devStatus_class();
    sdStatus_class sdStatus = new sdStatus_class();
    
    static Semaphore semaphorePort = new Semaphore(1);
    
    public Serial_port port;
    
    //Флаг обработки сообщений
    public byte USBC_PROC_FLAG_RET = 1; //Вернуть ответ
    public byte USBC_PROC_FLAG_NRET = 2; //Не возвращать ответ
    
    //Bootloader commands
    private final int USBC_BOOT_CMD_CONN = 1; //Connection
    private final int USBC_BOOT_CMD_RESET = 2; //Reset
    private final int USBC_BOOT_CMD_FWLOAD = 3; //FW load
            
    //Commands
    private final int USBC_CMD_DEBUG = 0; //Debug command
    private final int USBC_CMD_CONN_CHECK = 1; //Connection check
    private final int USBC_CMD_FLASH_WRITE = 2; //Write data to NAND
    private final int USBC_CMD_FLASH_READ = 3; //Read data from NAND
    private final int USBC_CMD_CHANGE_MODE = 4;  //Change mode
    private final int USBC_CMD_SCRYPT_LOAD = 5;  //Load script
    private final int USBC_CMD_SCRYPT_START = 6;  //Start script
    private final int USBC_CMD_SCRYPT_STOP = 7;  //Stop script
    private final int USBC_CMD_SCRYPT_PAUSE = 8;  //Pause script
    private final int USBC_CMD_SET_SETTINGS = 9;  //Set settings
    private final int USBC_CMD_GET_SETTINGS = 10;   //Get settings
    private final int USBC_CMD_ASSIGN_SETTINGS = 11; //Assign settings
    private final int USBC_CMD_DEFAULT_SETTINGS = 12; //Set default settings
    private final int USBC_CMD_SYSTEM_RESET = 13; //Reset
    private final int USBC_CMD_SET_CALIBR = 14;          //Set calibratings data
    private final int USBC_CMD_GET_CALIBR = 15;          //Get calibrate data
    private final int USBC_CMD_ASSIGN_CALIBR = 16;          //Assign calibr
    private final int USBC_CMD_GET_VALUES = 17;     //Get values
    private final int USBC_CMD_GET_GUID = 18;     //Get GUID
    private final int USBC_CMD_GET_ETH_IP = 19; //Get ETH IP
    private final int USBC_CMD_MODEM_SWITCH = 20; //Switch power modem
    private final int USBC_CMD_MODEM_TERMINAL_SWITCH = 21; //Terminal mode switch
    private final int USBC_CMD_MODEM_TERMINAL_SEND = 22; //Terminal send
    private final int USBC_CMD_MODEM_TERMINAL_READ = 23; //Terminal read    
    private final int USBC_CMD_WRITE_DISCRET = 24; //Write discrete io
    private final int USBC_CMD_READ_DISCRET = 25; //Read discrete io
    private final int USBC_CMD_RESET_COUNTERS = 26;  //Сбросить счетчики
    private final int USBC_CMD_GET_STATUS = 27;  //Получить статус устройства
    private final int USBC_CMD_GET_SD_STATUS = 28;  //Получить статус SD карты
    private final int USBC_CMD_OPENDIR = 29;    //Открыть директорию
    private final int USBC_CMD_FDIR_READ = 30;  //Прочитать информацию о файле/директории
    private final int USBC_CMD_MKDIR = 31;  //Создать новый каталог
    private final int USBC_CMD_DEL_DIR_FILE = 32; //Удалить папку/файл
    private final int USBC_CMD_CREATE_FILE = 33; //Созадть файл
    private final int USBC_CMD_IMPORT_FILE = 34; //Импорт файла
    private final int USBC_CMD_DEFAULT_CALIBRATE = 35; //Установить дефолтные калибровочные параметры

    //Modes
    public static int USBP_MODE_CMD = 0;
    public static int USBP_MODE_DEBUG = 1;
    public static int USBP_MODE_SCRIPT = 2;
    
    //Command status
    public static int USBC_STAT_EXEC_END = 0;
    public static int USBC_STAT_EXEC_START = 1;
    public static int USBC_STAT_EXEC_CONT = 2;

    //Return values
    public static final int USBC_RET_OK = 1;
    public static final int USBC_RET_ERR = 2;
    public static final int USBC_RET_CRC_ERR = 3;
    public static final int USBC_RET_ARD_ERR = 4;
    public static final int USBC_RET_OVERF = 5;
    public static final int USBC_RET_NAVAL = 6;
    public static final int USBC_RET_NEXIST = 7;        
    public static final int USBC_RET_ALREADY = 8;
    public static final int USBC_RET_END = 9;
            
    //Settings ID
    public static final int DC_SET_NET_MAC_ADR = 1;
    public static final int DC_SET_NET_DHCP_EN = 2;
    public static final int DC_SET_NET_DEV_IP = 3;
    public static final int DC_SET_NET_GW_IP = 4;
    public static final int DC_SET_NET_MASK = 5;
    public static final int DC_SET_NTP_DOMEN = 6;
    public static final int DC_SET_NET_DNS_IP = 7;
    public static final int DC_SET_MQTT_IP = 8;
    public static final int DC_SET_MQTT_DOMEN = 9;
    public static final int DC_SET_MQTT_CH = 10;
    public static final int DC_SET_MQTT_PORT = 11;
    public static final int DC_SET_MQTT_USER = 12;
    public static final int DC_SET_MQTT_PASS = 13;
    public static final int DC_SET_MQTT_QOS = 14;
    public static final int DC_SET_EMS_PERIOD = 15;
    public static final int DC_SET_EMS_AUTO_SEND = 16;
    public static final int DC_SET_EMS_CHANNEL_EN = 17;
    public static final int DC_SET_VM_AUTO_START = 18;

    //Список дикретных IO          
    public static final int DC_IO_DIN1 = 0;
    public static final int DC_IO_DIN2 = 1;
    public static final int DC_IO_DIN3 = 2;
    public static final int DC_IO_DIN4 = 3;
    public static final int DC_IO_K1 = 4;
    public static final int DC_IO_K2 = 5;
    public static final int DC_IO_K3 = 6;
    public static final int DC_IO_K4 = 7;
    
    //Открыть директорию
    public boolean openDir(String path)
    {
        Buf_class retVal;
        
        retVal = port.sendCMD((byte)USBC_CMD_OPENDIR, USBC_PROC_FLAG_RET, path.getBytes(), path.length(), 2000);
        
        if (retVal == null)
           return false;
        
        if (retVal.retStatus == USBC_RET_OK) //Если информация о файле пришла
           return true; 
        
        return false;
    }
    
    //Создать файл
    public boolean createSdFile(String fileSdPath)
    {
       Buf_class retVal;

       retVal = port.sendCMD((byte)USBC_CMD_CREATE_FILE, USBC_PROC_FLAG_RET, fileSdPath.getBytes(), fileSdPath.length(), 1000);
        
        if (retVal == null)
           return false;
       
        if (retVal.retStatus  == USBC_RET_OK)
           return true;
        
        return false;
    }
    
    //Отправить часть файла
    public boolean sendFilePart(int partsCount, int part, char[] data, int len)
    {
        Buf_class retVal;
        byte[] sendBuf = new byte[len+4];
        byte[] dataBuf = charArrToByteArr(data);
        sendBuf[0] = HI(partsCount);
        sendBuf[1] = LO(partsCount);
        sendBuf[2] = HI(part);
        sendBuf[3] = LO(part);
        
        System.arraycopy(dataBuf, 0, sendBuf, 4, len);
        
        retVal = port.sendCMD((byte)USBC_CMD_IMPORT_FILE, USBC_PROC_FLAG_RET, sendBuf, sendBuf.length, 2000);
        
        if (retVal == null)
           return false;
        
        if (retVal.retStatus == USBC_RET_OK)
            return true;
       
        return false;
    }
    
    //Удалить директорию/файл
    public boolean delDirFile(String path)
    {
        Buf_class retVal;
        
        retVal = port.sendCMD((byte)USBC_CMD_DEL_DIR_FILE, USBC_PROC_FLAG_RET, path.getBytes(), path.length(), 2000);
        
        if (retVal == null)
           return false;
        
        if (retVal.retStatus == USBC_RET_OK) //Если информация о файле пришла
           return true; 
        
        return false;
    }
    
    //Создать директорию
    public boolean mkDir(String path)
    {
        Buf_class retVal;
        
        retVal = port.sendCMD((byte)USBC_CMD_MKDIR, USBC_PROC_FLAG_RET, path.getBytes(), path.length(), 2000);
        
        if (retVal == null)
           return false;
        
        if (retVal.retStatus == USBC_RET_OK) //Если информация о файле пришла
           return true; 
        
        return false;
    }
            
    //Прочитать информацию о файле/директории
    public int fdirRead()
    {
        Buf_class retVal;
        byte[] fileSizeBuf = new byte[10];
        
        retVal = port.sendCMD((byte)USBC_CMD_FDIR_READ, USBC_PROC_FLAG_RET, null, 0, 2000);
        
        if (retVal == null)
            return -1;
        
        if (retVal.retStatus == USBC_RET_OK) //Если информация о файле пришла
        {
            fileInfo.fileName =  bytesToStr(retVal.payload, 0, 30);

            System.arraycopy(retVal.payload, 30, fileSizeBuf, 0, 4);
            fileInfo.fileSize = (int)(bytesToLong(fileSizeBuf));

            fileInfo.fileType = bytesToStr(retVal.payload, 34, 5);
 
        }
 
        return retVal.retStatus;
    }
    
    //Получить статус
    public boolean getSatus()
    {
        Buf_class retVal;
        
        retVal = port.sendCMD((byte)USBC_CMD_GET_STATUS, USBC_PROC_FLAG_RET, null, 0, 2000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            if ((retVal.payload[4] & 1) > 0)
                devStatus.ch1_active = true;
            
            if ((retVal.payload[4] & 2) > 0)
                devStatus.ch2_active = true;
            
            if ((retVal.payload[4] & 4) > 0)
                devStatus.ch3_active = true;
            
            if ((retVal.payload[4] & 8) > 0)
                devStatus.ch4_active = true;
            
            if ((retVal.payload[8] & 1) > 0)
                devStatus.sd_insert = true;
            
            if ((retVal.payload[9] & 1) > 0)
                devStatus.eth_link = true;
            
            if ((retVal.payload[11] & 1) > 0)
                devStatus.ems_link = true;
        }
        
        return true;
    }
    
    //Получить статус карты памяти
    public boolean getSdSatus()
    {
        byte[] bufBytes = new byte[10];
        Buf_class retVal;
        
        retVal = port.sendCMD((byte)USBC_CMD_GET_SD_STATUS, USBC_PROC_FLAG_RET, null, 0, 2000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            System.arraycopy(retVal.payload, 0, bufBytes, 0, 4);
            sdStatus.cap = (long)(bytesToLong(bufBytes));
            System.arraycopy(retVal.payload, 4, bufBytes, 0, 4);
            sdStatus.free = (long)(bytesToLong(bufBytes));
            System.arraycopy(retVal.payload, 8, bufBytes, 0, 4);
            sdStatus.fill = (long)(bytesToLong(bufBytes));
        }

        return true;
    }
            
    //Проверить ACK
    public boolean checkACK(byte[] data)
    {
        return ((data[0] == 'A') & (data[1] == 'C') & (data[2] == 'K'));
    }
    
    //Включить выключить модем
    public boolean modemSwitch(byte flagOn)
    {
        try {
            
            Buf_class retVal;
            byte[] modemData = new byte[1];
            modemData[0] = flagOn;
            
            semaphorePort.acquire();
            
            retVal = port.sendCMD((byte)USBC_CMD_MODEM_SWITCH, USBC_PROC_FLAG_RET, modemData, 1, 5000);
            
            semaphorePort.release();
                    
            if (retVal == null)
                return false;
            
            if (retVal.retStatus == USBC_RET_OK)
            {
                return true;
            }else{
                return false;
            }
            
        } catch (InterruptedException ex) {
            Logger.getLogger(ILC_device_c.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
            
    //Вывести в дебаг
    public boolean debugOut(String str) {
        Buf_class retVal;
        retVal = port.sendCMD((byte)USBC_CMD_DEBUG, USBC_PROC_FLAG_NRET, str.getBytes(), str.length(), 2000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
    
    //Connection check
    public boolean checkConnection() {
        Buf_class retVal;
        
        retVal = port.sendCMD((byte)USBC_CMD_CONN_CHECK, USBC_PROC_FLAG_RET, null, 0, 1000);
 
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
    
    //Change mode
    public boolean changeMode(int mode)
    {
        Buf_class retVal;
        byte[] modeData = new byte[2];
        modeData[0] = (byte)mode;
        
        retVal = port.sendCMD((byte)USBC_CMD_CHANGE_MODE, USBC_PROC_FLAG_RET, modeData, 1, 2000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
    
    //Сбросить счетчики
    public boolean resetCounters()
    {
        Buf_class retVal;
        
        retVal = port.sendCMD((byte)USBC_CMD_RESET_COUNTERS, USBC_PROC_FLAG_RET,  null, 0, 1000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }

    //Write discrete IO
    public boolean writeDiscreteIo(int id, int state)
    {
      Buf_class retVal;
        byte[] data = new byte[2]; 
        
        data[0] = (byte)id;
        data[1] = (byte)state;
    
        retVal = port.sendCMD((byte)USBC_CMD_WRITE_DISCRET, USBC_PROC_FLAG_RET, data, 2, 300);
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
            
    //Read discrete IO
    public int readDiscreteIo(int id)
    {
        Buf_class retVal;
        byte[] data = new byte[1];
        
        data[0] = (byte)id;
        
        retVal = port.sendCMD((byte)USBC_CMD_READ_DISCRET, USBC_PROC_FLAG_RET, data, 1, 300);
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            if (retVal.payload[0] == 1)
                return 1;
            else
                return 0;
        }else{
            return -1;
        }
    }
    
    //Switch modem terminal
    public boolean switchModemTerminal(boolean switchOn)
    {
        Buf_class retVal;
        byte[] data = new byte[1];
        
        if (switchOn)
            data[0] = 1;
        else
            data[0] = 0;
        
        retVal = port.sendCMD((byte)USBC_CMD_MODEM_TERMINAL_SWITCH, USBC_PROC_FLAG_RET, data, 1, 2000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
    
    //Modem terminal out
    public boolean modemTerminalOut(String str) {
        
        Buf_class retVal;
        retVal = port.sendCMD((byte)USBC_CMD_MODEM_TERMINAL_SEND, USBC_PROC_FLAG_RET, str.getBytes(), str.length(), 500);
        
        if (retVal == null)
            return false;
                    
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
    
    //Read terminal modem
    public String readTerminalModem()
    {
        Buf_class retVal;
        
        retVal = port.sendCMD((byte)USBC_CMD_MODEM_TERMINAL_READ, USBC_PROC_FLAG_RET, null, 0, 3000);
        
        if (retVal == null)
            return "";
        
        else
        {
            try {
                String str = new String(retVal.payload, "ASCII");
                if (retVal.payload[0] != 0)
                {
                    String strReplace = str.replaceAll(" ","");
                    return strReplace;
                }
                return "";
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ILC_device_c.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "";
    }
    
    //Read GUID
    public String readGuid()
    {
        try {
            Buf_class retVal;
            
            semaphorePort.acquire();
            retVal = port.sendCMD((byte)USBC_CMD_GET_GUID, USBC_PROC_FLAG_RET, null, 0, 500);
            semaphorePort.release();
            
            if (retVal == null)
                return "";
            else
            {
                try {
                    String str = new String(retVal.payload, "UTF-8");
                    String strReplace = str.replaceAll(" ","");
                    return strReplace;
                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(ILC_device_c.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return "";
        } catch (InterruptedException ex) {
            Logger.getLogger(ILC_device_c.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "";
    }
    
    //Read ETH IP
    public String readEthIp()
    {
        try {
            Buf_class retVal;
            
            semaphorePort.acquire();
            retVal = port.sendCMD((byte)USBC_CMD_GET_ETH_IP, USBC_PROC_FLAG_RET, null, 0, 200);
            semaphorePort.release();
            
            if (retVal == null)
                return "";
            else
            {
                String ip = Integer.toString((int)retVal.payload[0] & 0xFF) + "."
                        + Integer.toString((int)retVal.payload[1] & 0xFF) + "."
                        + Integer.toString((int)retVal.payload[2] & 0xFF) + "."
                        + Integer.toString((int)retVal.payload[3] & 0xFF);
                return ip;
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(ILC_device_c.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "";
    }
    
    //Return buffer
    public class ILC_buf_c {
        public byte[] Data = new byte[300];
        public int Len = 0;
        public int status;
    }
    
    //Write NAND data
    public Buf_class writeNandData(int block, int page, int offset, byte[] data, int len)
    {
        Buf_class retVal;
        byte[] payload = new byte[300];
        payload[0] = HI(block);
        payload[1] = LO(block);
        
        payload[2] = HI(page);
        payload[3] = LO(page);
        
        payload[4] = HI(offset);
        payload[5] = LO(offset);
        
        payload[6] = HI(len);
        payload[7] = LO(len);
        
        System.arraycopy(data, 0, payload, 8, len);
        
        retVal = port.sendCMD((byte)USBC_CMD_FLASH_WRITE, USBC_PROC_FLAG_RET, payload, len+8, 2000);
        
        return retVal;
    }
    
    //Read NAND data
    public Buf_class readNandData(int block, int page, int offset, int len)
    {
        Buf_class retVal;
        
        byte[] payload = new byte[300];
        payload[0] = HI(block);
        payload[1] = LO(block);
        
        payload[2] = HI(page);
        payload[3] = LO(page);
        
        payload[4] = HI(offset);
        payload[5] = LO(offset);
        
        payload[6] = HI(len);
        payload[7] = LO(len);
        
        retVal = port.sendCMD((byte)USBC_CMD_FLASH_READ, USBC_PROC_FLAG_RET, payload, 8, 2000);
        
        return retVal;
    }
    
    //Reset device
    public boolean resetDevice()
    {
        Buf_class retVal;
        retVal = port.sendCMD((byte)USBC_CMD_SYSTEM_RESET, USBC_PROC_FLAG_RET, null, 0, 2000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
    
    //Load scrypt
    public boolean loadScypt(String name, byte[] script, long len)
    {
        int maxLenPart = 256;
        Buf_class retVal;
        byte[] dataScript = new byte[300];
        int countParts = (int) floor(len/maxLenPart);
        int tailLen = (int) (len - countParts*maxLenPart);
        byte[] nameBuf = new byte[20];
        byte[] ctcVal = new byte[2];
        CRC16_c crc16 = new CRC16_c();
        
        //Send info data
        System.arraycopy(name.getBytes(), 0, nameBuf, 0, name.getBytes().length);
        crc16.calc(ctcVal, script, len);
 
        dataScript[0] = (byte) USBC_STAT_EXEC_START;
        dataScript[1] = HI(len);
        dataScript[2] = LO(len);
        dataScript[3] = ctcVal[1];
        dataScript[4] = ctcVal[0];
        System.arraycopy(nameBuf, 0, dataScript, 5, 20);
        
        retVal = port.sendCMD((byte)USBC_CMD_SCRYPT_LOAD, USBC_PROC_FLAG_RET, dataScript, 25, 4000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus != USBC_RET_OK)
            return false;
        
        //Full Parts
        for (int i = 0; i < countParts; i++) {
            
            dataScript[0] = (byte) USBC_STAT_EXEC_CONT;
            dataScript[1] = HI(i);
            dataScript[2] = LO(i);
            dataScript[3] = HI(maxLenPart);
            dataScript[4] = LO(maxLenPart);
            
            
            System.arraycopy(script, i*maxLenPart, dataScript, 5, maxLenPart);
            
            retVal = port.sendCMD((byte)USBC_CMD_SCRYPT_LOAD, USBC_PROC_FLAG_RET, dataScript, maxLenPart+5, 4000);
            
            if (retVal.retStatus != USBC_RET_OK)
                return false;
        }

        //Tail
        dataScript[0] = (byte)USBC_STAT_EXEC_END;
        dataScript[1] = HI(countParts);
        dataScript[2] = LO(countParts);
        dataScript[3] = HI(tailLen);
        dataScript[4] = LO(tailLen);
            
        System.arraycopy(script, countParts*maxLenPart, dataScript, 5, tailLen);
        retVal = port.sendCMD((byte)USBC_CMD_SCRYPT_LOAD, USBC_PROC_FLAG_RET, dataScript, tailLen+5, 10000);
        
        if (retVal.retStatus != USBC_RET_OK)
            return false;
        
        return true;
    }
    
    //Start scrypt
    public boolean StartScrypt()
    {
        Buf_class retVal;
        retVal = port.sendCMD((byte)USBC_CMD_SCRYPT_START, USBC_PROC_FLAG_RET, null, 0, 1000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
    
    //Stop scrypt
    public boolean StopScrypt()
    {
        Buf_class retVal;
        retVal = port.sendCMD((byte)USBC_CMD_SCRYPT_STOP, USBC_PROC_FLAG_RET, null, 0, 1000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }

    //Pause scrypt
    public boolean PauseScrypt()
    {
        Buf_class retVal;
        retVal = port.sendCMD((byte)USBC_CMD_SCRYPT_PAUSE, USBC_PROC_FLAG_RET, null, 0, 1000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
    
    public ILC_buf_c readSettingParam(int setID)
    {
        ILC_buf_c retBuf = new ILC_buf_c();
        Buf_class retVal;
        byte[] payload = new byte[20];

        payload[0] = (byte) setID;
     
        retVal = port.sendCMD((byte)USBC_CMD_GET_SETTINGS, USBC_PROC_FLAG_RET, payload, 1, 1000);

        retBuf.status = retVal.retStatus;
        
        if (retVal == null)
            return null;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            retBuf.Len = retVal.payload[0];
            System.arraycopy(retVal.payload, 1, retBuf.Data, 0, retBuf.Len);
        }

        return  retBuf;
    }
    
    public boolean writeSettingParam(int setID, byte[] data, int len)
    {
        byte[] payload = new byte[300];
        Buf_class retVal;
        
        payload[0] = (byte) setID;
        payload[1] = (byte)len;
        System.arraycopy(data, 0, payload, 2, len);
        
        retVal = port.sendCMD((byte)USBC_CMD_SET_SETTINGS, USBC_PROC_FLAG_RET, payload, len+2, 1000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
    
    //Assign settings
    public boolean assignSettings()
    {
        Buf_class retVal;
        retVal = port.sendCMD((byte)USBC_CMD_ASSIGN_SETTINGS, USBC_PROC_FLAG_RET, null, 0, 2000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
    
    //Reset settings
    public boolean resetSettings()
    {
        Buf_class retVal;
        retVal = port.sendCMD((byte)USBC_CMD_DEFAULT_SETTINGS, USBC_PROC_FLAG_RET, null, 0, 2000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }

    //Reset calibrate
    public boolean resetCalibrate()
    {
        Buf_class retVal;
        retVal = port.sendCMD((byte)USBC_CMD_DEFAULT_CALIBRATE, USBC_PROC_FLAG_RET, null, 0, 2000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
    
    public ILC_buf_c readValues(int valID, int channel, int line)
    {
        ILC_buf_c retBuf = new ILC_buf_c();
        Buf_class retVal;
        byte[] payload = new byte[20];

        payload[0] = (byte) valID;
        payload[1] = (byte) channel;
        payload[2] = (byte) line;
     
        retVal = port.sendCMD((byte)USBC_CMD_GET_VALUES, USBC_PROC_FLAG_RET, payload, 3, 1000);

        if (retVal == null)
            return null;
            
        retBuf.status = retVal.retStatus;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            retBuf.Len = retVal.payload[0];
            System.arraycopy(retVal.payload, 1, retBuf.Data, 0, retBuf.Len);
        }

        return  retBuf;
    }

    public ILC_buf_c readCal(int calID, int channel, int line)
    {
        ILC_buf_c retBuf = new ILC_buf_c();
        Buf_class retVal;
        byte[] payload = new byte[20];

        payload[0] = (byte) calID;
        payload[1] = (byte) channel;
        payload[2] = (byte) line;
     
        retVal = port.sendCMD((byte)USBC_CMD_GET_CALIBR, USBC_PROC_FLAG_RET, payload, 3, 700);

        if (retVal == null)
            return null;
        
        retBuf.status = retVal.retStatus;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            retBuf.Len = retVal.payload[0];
            System.arraycopy(retVal.payload, 1, retBuf.Data, 0, retBuf.Len);
        }

        return  retBuf;
    }
    
    public boolean writeCal(int calID, int channel, int line, byte[] data)
    {
        byte[] payload = new byte[300];
        Buf_class retVal;
        
        payload[0] = (byte) calID;
        payload[1] = (byte) channel;
        payload[2] = (byte) line;
        payload[3] = 4;
        
        System.arraycopy(data, 0, payload, 4, 4);
        
        retVal = port.sendCMD((byte)USBC_CMD_SET_CALIBR, USBC_PROC_FLAG_RET, payload, 8, 1000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
    
    //Assign calibr
    public boolean assignCalibr()
    {
        Buf_class retVal;
        retVal = port.sendCMD((byte)USBC_CMD_ASSIGN_CALIBR, USBC_PROC_FLAG_RET, null, 0, 10000);
        
        if (retVal == null)
            return false;
        
        if (retVal.retStatus == USBC_RET_OK)
        {
            return true;
        }else{
            return false;
        }
    }
    
    public ILC_device_c(Serial_port serialport) {
        port = serialport;
    }
    
    public String bytesToStr(byte[] data, int start, int count)
    {
        try {
            byte[] byteBuf = new byte[256];
            int endpos = 0;
            
            System.arraycopy(data, start, byteBuf, 0, count);
            String buf = new String(byteBuf, "UTF-8");
            for (int i=buf.length()-1; i != 0; i--)
            {
                if (buf.charAt(i) != ' ')
                {
                  endpos = i+1;
                  break;
                }
            }

            return buf.substring(0, endpos);
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ILC_device_c.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public byte[] LongToBytes(long l) {
        byte[] result = new byte[4];
        for (int i = 0; i < 4; i++) {
            result[i] = (byte) (l & 0xFF);
            l >>= 8;
        }
        return result;
    }

    public long bytesToLong(byte[] bytes) {
        long result = 0;
        for (int i = 0; i < 4; i++) {
            result <<= 8;
            result |= (bytes[3-i] & 0xFF);
        }
        return result;
    }
    
    public int bytesToInt(byte[] bytes) {
        int result = 0;
        for (int i = 0; i < 2; i++) {
            result <<= 8;
            result |= (bytes[3-i] & 0xFF);
        }
        return result;
    }
    public byte[] charArrToByteArr(char[] value)
    {
        byte[] result = new byte[value.length];
        
        for (int i = 0; i < value.length; i++)
            result[i] = (byte) value[i];
        
        return result;
    }

    public float bytesToFloat(byte[] bytes) {
        int intBits = bytes[3] << 24 | (bytes[2] & 0xFF) << 16 | (bytes[1] & 0xFF) << 8 | (bytes[0] & 0xFF);
        return Float.intBitsToFloat(intBits);
    }

    public byte[] floatToBytes(float value) {
        int intBits = Float.floatToIntBits(value);
        return new byte[]{
            (byte) (intBits), (byte) (intBits >> 8), (byte) (intBits >> 16), (byte) (intBits >> 24)};
    }

    public byte HI(long value) {
       return  (byte) ((value & 0xFF00) >> 8);
    }
    
    public byte LO(long value) {
       return  (byte) (value & 0x00FF) ;
    }
    
}
