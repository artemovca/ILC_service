/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ILC_service;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

/**
 *
 * @author user
 */
public class Serial_port {
    
    //Стартовый символ
    public byte START_BYTE = 0x55;

    //Тип сообщения
    public byte MES_CMD = 1;    //Команда
    public byte MES_DATA = 2;   //Данные
    public byte MES_ACK = 3;   //Ответ

    //Флаг обработки сообщений
    public byte PROC_FLAG_RET = 1; //Вернуть ответ
    public byte PROC_FLAG_NRET = 2; //Не возвращать ответ
    
    public COM_data_с COM_data; //Класс данных
    public COM_thead_с COM_thead; //Поток работы с портом
    public boolean runThread = true;
     
    //Конструктор
    Serial_port(){
        COM_data = new COM_data_с();
    }
    
    public static class COM_data_с {
        
        public int packRecive = 0; //recive flag
        public int dataRecive = 0; //recive flag
        public int state  = 0; //Состояние COM порта
        public final int rxMAX_len = 300;
        public byte[] rx_buf = new byte[rxMAX_len]; //Буфер приема;
        public int rx_len = 0;  //Счетчик
        public static SerialPort serialPort;
        public int Mode;
        public static int MODE_PROTOCOL = 0;
        public static int MODE_TRANSPARENT = 1;
    }
    
    public class COM_thead_с extends Thread {

     
        @Override
        public void run() {
            synchronized(COM_data) {
                try {
                    COM_data.serialPort.addEventListener(new EventListener(), SerialPort.MASK_RXCHAR);
                } catch (SerialPortException ex) {
                    Logger.getLogger(ILC_main_form.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            while (runThread) {

            }
        }
        
        public void stopRunThread()
        {
            runThread = false;
        }

        //Событие приема данных
        private class EventListener implements SerialPortEventListener {

            @Override
            public void serialEvent(SerialPortEvent event) {
                if (event.isRXCHAR() && event.getEventValue() > 0) {

                    try {

                        //Protocol mode
                        if (COM_data.Mode == COM_data.MODE_PROTOCOL) {
                            //Check process
                            if (COM_data.packRecive == 1) {
                                Thread.sleep(500);
                            }

                            if (COM_data.packRecive == 1) {
                                return;
                            }

                            //Получить данные
                            int len = event.getEventValue();
                            byte[] data = COM_data.serialPort.readBytes(len);

                            //Счетчик принятых байт
                            if ((COM_data.rx_len + len) < COM_data.rxMAX_len) {

                                System.arraycopy(data, 0, COM_data.rx_buf, COM_data.rx_len, len);
                                COM_data.rx_len += len;

                            } else {
                                Arrays.fill(COM_data.rx_buf, (byte) 0);
                                COM_data.rx_len = 0;
                                COM_data.serialPort.readBytes();
                            }

                            //PArce package
                            if (COM_data.rx_len > 4) {

                                //Найти стоповые байты в массиве
                                for (int i = 0; i < COM_data.rx_len - 1; i++) {

                                    //Если стоповые байты найдены
                                    //if ((COM_data.rx_buf[i] == stop_1) && (COM_data.rx_buf[i + 1] == stop_2)) {

                                        COM_data.packRecive = 1; //Пойман пакет
                                        COM_data.serialPort.readBytes();
                                    //}
                                }
                            }
                        }
                        
                        //Transparent mode
                        if (COM_data.Mode == COM_data.MODE_TRANSPARENT) {
                            
                            //Check process
                            if (COM_data.dataRecive == 1) {
                                Thread.sleep(5);
                            }

                            if (COM_data.dataRecive == 1) {
                                return;
                            }
                            
                            //Получить данные
                            int len = event.getEventValue();
                            byte[] data = COM_data.serialPort.readBytes(len);

                            //Счетчик принятых байт
                            if ((COM_data.rx_len + len) < COM_data.rxMAX_len) {

                                System.arraycopy(data, 0, COM_data.rx_buf, COM_data.rx_len, len);
                                COM_data.rx_len += len;
                                COM_data.serialPort.readBytes();
                                COM_data.dataRecive = 1;

                            } else {
                                Arrays.fill(COM_data.rx_buf, (byte) 0);
                                COM_data.rx_len = 0;
                                COM_data.serialPort.readBytes();
                            }

                        }


                    } catch (SerialPortException ex) {
                        Logger.getLogger(Serial_port.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Serial_port.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }

            }
        }
    }

    //Открыть COM порт
    public boolean openCOMPort(String COM_in, int COM_speed) {
        boolean val = false;
        
        COM_data.serialPort = new SerialPort(COM_in);
        try {
            COM_data.serialPort.openPort();
            COM_data.serialPort.setParams(COM_speed, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            COM_thead = new COM_thead_с();
            COM_thead.start();
            COM_thead.setPriority(2);
            COM_data.state = 1; //COM порт открыт
            val = true;
        }
        catch (SerialPortException ex) {
            System.out.println(ex);
        }
        return val;
    }
    
    //Закрыть порт
    public boolean closeCOMPort() {
        boolean val = false;
        
        try {
            COM_data.serialPort.closePort();
            COM_data.state = 0; //COM порт закрыт
            //COM_thead.stop();
            COM_thead.stopRunThread();
            COM_thead = null;
            val = true;
        } catch (SerialPortException ex) {
            System.out.println(ex);
        }
        return val;
    }
    
    //Отправить байты в COM порт
    public int writeBytes(byte array[], int len) {
        int partLen = 64;
        int parts = len/partLen;
        int tail = len - parts*partLen;
        
        for(int i=0; i<parts; i++)
        {
            byte[] array_toSend = new byte[partLen];
            System.arraycopy(array, partLen*i, array_toSend, 0, partLen);
            try {
                COM_data.serialPort.writeBytes(array_toSend);
                Thread.sleep(20);
                array_toSend = null;
            } catch (SerialPortException ex) {
                array_toSend = null;
                return 0;
            } catch (InterruptedException ex) {
                Logger.getLogger(Serial_port.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }

        if (tail > 0)
        {
            byte[] array_toSend = new byte[tail];
            System.arraycopy(array, partLen*parts, array_toSend, 0, tail);
            try {
                COM_data.serialPort.writeBytes(array_toSend);
            } catch (SerialPortException ex) {
                return 0;
            }
        }

        return 1;
    }
    
    //Отправить байты в COM порт
    public void writeBytes(byte array[]) {
        try {
            COM_data.serialPort.writeBytes(array);
        } catch (SerialPortException ex) {
            System.out.println(ex);
        }
    }

    //Return buffer
    public class Buf_class {
        public byte[] payload = new byte[300];
        public int payloadLen = 0;
        public boolean recived = false;
        public byte retStatus = 0;
        public byte cmdID = 0;
    }
    
    //Send command return ret data len
    public Buf_class sendCMD(byte cmd, byte procFlag, byte[] payload, int payloadLen, int wait) {

        Buf_class retVal = new Buf_class(); //Буфер принятых данных
        byte[] crc_val = new byte[2];
        byte[] packCrc = new byte[2];
        CRC16_c CRC16 = new CRC16_c(); //Класc для расчета CRC16
        byte[] sendData = new byte[256]; //Буфер данных отправления

        sendData[0] = START_BYTE;
        sendData[1] = MES_CMD;
        sendData[2] = cmd;
        sendData[3] = procFlag;
        sendData[4] = (byte) ((payloadLen & 0xFF00) >> 8);
        sendData[5] = (byte) (payloadLen & 0xFF);
        sendData[6] = 0;
        sendData[7] = 0;
        
        //Если длина больше нуля и данные существуют
        if ((payloadLen > 0) && (payload != null))
            System.arraycopy(payload, 0, sendData, 8, payloadLen);

        CRC16.calc(crc_val, sendData, payloadLen+8);
       
        sendData[6] = crc_val[1];
        sendData[7] = crc_val[0];
        
        if (writeBytes(sendData, payloadLen + 8) == 0)
            return null;

        //Если ответа ждать не нужно, то возвращаем Null
        if (procFlag == PROC_FLAG_NRET)
            return null;
            
        int try_counter = 0;
        COM_data.packRecive = 0;

        try {
            while (COM_data.packRecive != 1) {

                if (try_counter >= (wait/3)) {
                    break;
                }
                try_counter++;
                Thread.sleep(3);

            }
        } catch (InterruptedException ex) {
            Logger.getLogger(Serial_port.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Если пакет пришел
        if (COM_data.packRecive == 1) {

            //Проверить тип пакета
            if (COM_data.rx_buf[1] == MES_ACK)
            {
                packCrc[1] = COM_data.rx_buf[6];
                packCrc[0] = COM_data.rx_buf[7];
                COM_data.rx_buf[6] = 0;
                COM_data.rx_buf[7] = 0;

                //Проверка ответа
                crc_val[0] = 0;
                crc_val[1] = 0;

                //Расчет CRC
                CRC16.calc(crc_val, COM_data.rx_buf, COM_data.rx_len);

                //Проверка CRC
                if ((crc_val[0] == packCrc[0]) & (crc_val[1] == packCrc[1])) {
                    retVal.cmdID = COM_data.rx_buf[2];
                    retVal.retStatus = COM_data.rx_buf[3];
                    retVal.payloadLen = (COM_data.rx_buf[4] << 8) | COM_data.rx_buf[5];

                    if (retVal.payloadLen > 0)
                        System.arraycopy(COM_data.rx_buf, 8, retVal.payload, 0, retVal.payloadLen);

                    retVal.recived = true;
                }else{
                  retVal.recived = false;  
                }
            }else{
              retVal.recived = false;  
            }
        }else{
            retVal.recived = false;
        }
        
        COM_data.packRecive = 0;
        COM_data.rx_len = 0;
            
        return retVal;
    }
}